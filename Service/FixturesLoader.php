<?php

namespace Callers\FixturesBundle\Service;

use Callers\FixturesBundle\Service\Loader\LoaderFactory;
use Nelmio\Alice\Loader\NativeLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

class FixturesLoader
{
    private ContainerInterface $container;
    private array $configs;

    public function __construct(ContainerInterface $container, array $configs = [])
    {
        $this->container = $container;
        $this->configs   = $configs;
    }

    public function loadFixtures(): void
    {
        foreach ($this->configs as $config) {
            $loader = LoaderFactory::getLoader($config['type'], $config);

            $managerService = $loader->getManagerService();

            if (!$this->container->has($managerService)) {
                continue;
            }

            $manager = $this->container->get($managerService);

            $loader->setManager($manager);

            $loader->resetDatabase();

            $finder = new Finder();
            $files = $finder
                ->ignoreDotFiles(true)
                ->files()
                ->in($config['fixtures_path']);

            $loader = new NativeLoader();

            foreach ($files as $file) {
                $objects = $loader->loadFile($file->getRealPath());

                foreach ($objects->getObjects() as $object) {
                    $manager->persist($object);
                }

                $manager->flush();
            }
        }
    }
}
