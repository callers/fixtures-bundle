<?php

namespace Callers\FixturesBundle\Service\Loader;

interface LoaderInterface
{
    /**
     * Resets the database linked to the loader
     */
    public function resetDatabase(): void;

    /**
     * Returns the string corresponding to the manager in the Symfony container
     */
    public function getManagerService(): string;

    /**
     * Sets the manager property in the class
     * @param mixed $manager
     */
    public function setManager($manager): void;
}
