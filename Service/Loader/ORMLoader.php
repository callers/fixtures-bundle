<?php

namespace Callers\FixturesBundle\Service\Loader;

use Symfony\Component\Process\Process;

class ORMLoader extends AbstractLoader
{
    /**
     * Delete all the entries from the relational database then update the schema
     */
    public function resetDatabase(): void
    {
        $connectionName = $this->config['connection'] ?? 'default';

        $process = new Process(['bin/console', 'doctrine:database:drop', '--force', '--connection=' . $connectionName]);
        $process->run();

        $process = new Process(['bin/console', 'doctrine:database:create', '--connection=' . $connectionName]);
        $process->run();

        $process = new Process(['bin/console', 'doctrine:schema:update',  '--force', '--em=' . $connectionName]);
        $process->run();
    }

    /** @inheritDoc */
    public function getManagerService(): string
    {
        return 'doctrine.orm.' . $this->config['connection'] . '_entity_manager';
    }
}
