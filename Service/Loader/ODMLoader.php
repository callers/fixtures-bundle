<?php

namespace Callers\FixturesBundle\Service\Loader;

use Symfony\Component\Process\Process;

class ODMLoader extends AbstractLoader
{
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /**
     * Delete the documents from MongoDB database then update the schemas
     */
    public function resetDatabase(): void
    {
        $databaseName           = $this->manager->getConfiguration()->getDefaultDB();
        $connectionName         = $this->config['connection'] ?? 'default';
        $collectionClassMapping = $this->mapCollectionToClass();

        $client      = $this->manager->getClient();
        $database    = $client->selectDatabase($databaseName);
        $collections = $database->listCollections();

        foreach ($collections as $collection) {
            if (in_array($collection->getName(), $this->config['ignored'] ?? [])) {
                continue;
            }

            $database->selectCollection($collection->getName())->deleteMany([]);
            $class = $collectionClassMapping[$collection->getName()] ?? null;

            if (empty($class)) {
                continue;
            }

            $process = new Process([
                'bin/console',
                'doctrine:mongodb:schema:update',
                '--dm=' . $connectionName,
                '--class=' . $class,
            ]);

            $process->run();
        }
    }

    /** @inheritDoc */
    public function getManagerService(): string
    {
        return 'doctrine_mongodb.odm.' . $this->config['connection'] . '_document_manager';
    }

    /**
     * Create an associative array between the name of the collection as the key
     * and the name of the associated class as the value
     *
     * @return string[]
     */
    private function mapCollectionToClass(): array
    {
        $mapping = [];
        $classNames = $this->manager->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();

        foreach ($classNames as $className) {
            $classMetaData = $this->manager->getClassMetadata($className);
            $mapping[$classMetaData->getCollection()] = $classMetaData->getName();
        }

        return $mapping;
    }
}
