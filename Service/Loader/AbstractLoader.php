<?php

namespace Callers\FixturesBundle\Service\Loader;

class AbstractLoader implements LoaderInterface
{
    protected array $config;

    /** @var mixed|null */
    protected $manager;

    public function __construct(array $config)
    {
        $this->config  = $config;
    }

    public function resetDatabase(): void
    {
    }

    public function getManagerService(): string
    {
        return '';
    }

    /** @param mixed|null $manager */
    public function setManager($manager): void
    {
        $this->manager = $manager;
    }
}
