<?php

namespace Callers\FixturesBundle\Service\Loader;

use LogicException;

class LoaderFactory
{
    public static function getLoader(string $type, array $config): LoaderInterface
    {
        switch ($type) {
            case 'orm' === $type:
                return new ORMLoader($config);
            case 'odm' === $type:
                return new ODMLoader($config);
            default:
                new LogicException('Valid types are "orm" and "odm"');
        }
    }
}
