<?php

namespace Callers\FixturesBundle\Command;

use Callers\FixturesBundle\Service\FixturesLoader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadFixturesCommand extends Command
{
    protected static $defaultName = 'callers:fixtures:load';
    private FixturesLoader $fixturesLoader;

    public function __construct(FixturesLoader $fixturesLoader)
    {
        parent::__construct();
        $this->fixturesLoader = $fixturesLoader;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->fixturesLoader->loadFixtures();

        return 0;
    }
}
