<?php

namespace Callers\FixturesBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('callers_fixtures');
        $root    = $builder->getRootNode();

        $root
            ->children()
                ->arrayNode('loaders')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->treatNullLike([])
                        ->children()
                            ->scalarNode('manager')->end()
                            ->scalarNode('type')->end()
                            ->scalarNode('connection')->end()
                            ->scalarNode('fixtures_path')->end()
                            ->arrayNode('ignored')
                                ->scalarPrototype()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $builder;
    }
}
