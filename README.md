# Callers Fixture Bundle

# Installation

## Applications that use Symfony Flex

Open a command console, enter your project directory and execute:

```console
$ composer require --dev callers/fixtures-bundle
```

## Applications that don't use Symfony Flex

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require --dev callers/fixtures-bundle
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new Callers\FixturesBundle\CallersFixturesBundle(),
        ];

        // ...
    }

    // ...
}
```

# Configuration

To configure the package, you can create a file `config/packages/%env%/callers_fixtures.yaml`.

The configuration looks like this:

```yaml
callers_fixtures:
  loaders:
    mongodb:
      type: odm
      connection: default
      fixtures_path: "%kernel.project_dir%/tests/fixtures/mongo"
      ignored: ["my_ignored_collection"]
    mysql:
      type: orm
      connection: default
      fixtures_path: "%kernel.project_dir%/tests/fixtures/mysql"
```

The keys `mongodb` and `mysql` under the `loaders` property are arbitrary and can be anything, these are just the names you give to your loaders.

### Keys descriptions

Each loader can be configured with the following properties :

- **type :** it can be `odm` or `orm` depending on what you use from Doctrine.
- **connection :** the name of the manager you use for this set of features. If blank, it will defaults to `default`.
- **fixtures_path :** the path to the fixtures folder for this loader.
- **ignored :** the tables/collections you want to ignore when loading the fixtures, useful if your data is coming from a dump and you don't want to delete the data and update the schema of this table. **/!\\ It is currently supported only for the ODM /!\\**
